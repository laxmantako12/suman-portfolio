$(function() {
    //smooth scrolling from css-tricks
    $('a[href*="#"]:not([href="#"])').click(function() {

        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);

            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top-100
                }, 1000);
                return false;
            }
        }
    });

//from next page scroll
    var hash= window.location.hash
    if ( hash == '' || hash == '#' || hash == undefined ) return false;            
          var target = $(hash);        
          headerHeight = 120;          
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');          
          if (target.length) {
            $('html,body').stop().animate({
              scrollTop: target.offset().top - 125 //offsets for fixed header
            }, 'linear');
            
          }

});