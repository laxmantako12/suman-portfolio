$(function(){

    $('.hamburger-icon').on('click', function(){
        $(this).toggleClass('close-icon');
        $('body').toggleClass('body-fix');
    });
    $('.menu-item a').on('click', function(){
        $('.navbar-collapse').toggleClass('show');
        $('.hamburger-icon').toggleClass('close-icon');
        $('body').removeClass('body-fix');
    });


    $('.packages-block').matchHeight();
    $('.packages-block h5').matchHeight();
    $('.packages-block p').matchHeight();
    $('.services-mid-blockk p').matchHeight();

    $('.logo-slider-wrapper').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
            breakpoint: 576,
            settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
            breakpoint: 415,
            settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    $('.clients-slider').slick({
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow:'<button type="button" class="slick-prev pull-left"><svg xmlns="http://www.w3.org/2000/svg" width="13.1" height="20.4" viewBox="0 0 13.1 20.4"><g id="_1" data-name=" 1" transform="translate(13.1 20.4) rotate(180)"><path id="Path_36" data-name="Path 36" d="M10.2,13.1,0,3.007,3.038,0,10.2,7.087,17.362,0,20.4,3.007Z" transform="translate(0 20.4) rotate(-90)" fill="#272381"/></g></svg></button>',
        nextArrow:'<button type="button" class="slick-next pull-right"><svg id="Arrow_Small_Right_1" data-name="Arrow Small Right – 1" xmlns="http://www.w3.org/2000/svg" width="13.1" height="20.4" viewBox="0 0 13.1 20.4"><path id="Path_36" data-name="Path 36" d="M10.2,13.1,0,3.007,3.038,0,10.2,7.087,17.362,0,20.4,3.007Z" transform="translate(0 20.4) rotate(-90)" fill="#272381"/></svg></button>',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    centerMode: false,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });

    $('#main-slider').slick({
        dots: true,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    // submenu for mobile
    $('.menu-item-has-children').prepend('<span class="drop-toggler">&#9662</span>');

    $('body').on('click', '.drop-toggler', function () {
        // $('.sub-menu').slideUp();
        $(this).parents('.menu-item-has-children').find('.sub-menu').slideToggle();
        $(this).toggleClass('open');
    });



    $('.testimonial-content-slider').slick({
        slidesToShow: 1,
        arrows: true,
        dots: true,
        autoplay: false,
        prevArrow:'<button type="button" class="slick-prev pull-left"><svg xmlns="http://www.w3.org/2000/svg" width="13.1" height="20.4" viewBox="0 0 13.1 20.4"><g id="_1" data-name=" 1" transform="translate(13.1 20.4) rotate(180)"><path id="Path_36" data-name="Path 36" d="M10.2,13.1,0,3.007,3.038,0,10.2,7.087,17.362,0,20.4,3.007Z" transform="translate(0 20.4) rotate(-90)" fill="#272381"/></g></svg></button>',
        nextArrow:'<button type="button" class="slick-next pull-right"><svg id="Arrow_Small_Right_1" data-name="Arrow Small Right – 1" xmlns="http://www.w3.org/2000/svg" width="13.1" height="20.4" viewBox="0 0 13.1 20.4"><path id="Path_36" data-name="Path 36" d="M10.2,13.1,0,3.007,3.038,0,10.2,7.087,17.362,0,20.4,3.007Z" transform="translate(0 20.4) rotate(-90)" fill="#272381"/></svg></button>',
    });

    $('.testimonial-content-slider .slick-dots').appendTo('.testimonial-item-left');

    $(".text-video a.triger-popup").fancybox({
        iframe: {
            preload: false,
        },
        helpers: {
            media: true
        },
        media: {
            youtube: {
                params: {
                    autoplay: 1,

                }
            }
        }
    });

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 110
        }, 500);
    });

});

$(document).ready(function(){
    AOS.init();
})
